﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverTextAnimator : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var transformCache = transform; 
        var defaultPosition = transformCache.localPosition; // 終点として使用するため、初期座標を保持
        transform.localPosition = new Vector3(0, 300f); // いったん上の方に移動させる
        transformCache.DOLocalMove(defaultPosition, 1f)
            .SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                Debug.Log("GAME OVER!!");
                transformCache.DOShakePosition(1.5f, 100);  // シェイクアニメーション
            }
            );

        DOVirtual.DelayedCall(10, () =>       // DOTweenには、Coroutineを使わずに任意の秒数を持てる便利メソッドも搭載されている。
        {
            SceneManager.LoadScene("TitleScene");   // 10秒待ってからタイトルシーンに遷移
        }
        );

    }

   
}
