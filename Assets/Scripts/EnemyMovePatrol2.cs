﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovePatrol2 : MonoBehaviour
{

    private NavMeshAgent _agent;
    private GameObject destination;  // Agentの目的地となるQuadオブジェクトを格納
    public GameObject[] patrolPoints = new GameObject[4];   // Agentの目的地として登録するPatrolPointプレファブ

    // Start is called before the first frame update
    void Start()
    {
        _agent = GetComponent<NavMeshAgent>();      // NavMeshAgentを保持しておく 
        destination = patrolPoints[0];      // destinationの初期化　最初にPatrolPoint5の座標を代入する
    }


    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "PatrolPoint5":
                destination = patrolPoints[1];  //destinationにPatrolPoint6を代入
                break;

            case "PatrolPoint6":
                destination = patrolPoints[2];  //destinationにPatrolPoint7を代入
                break;

            case "PatrolPoint7":
                destination = patrolPoints[3];  //destinationにPatrolPoint8を代入
                break;

            case "PatrolPoint8":
                destination = patrolPoints[0];  //destinationにPatrolPoint5を代入
                break;
        }
    }


    // Update is called once per frame
    void Update()
    {
        _agent.destination = destination.transform.position;
    }
}
