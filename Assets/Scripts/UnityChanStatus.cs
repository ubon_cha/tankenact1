﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UnityChanStatus : MonoBehaviour
{

    // 敵と接触した時にゲームオーバー画面に遷移させる
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Enemy")       
        {
            StartCoroutine(GoToGameOverCoroutine());
        }

    }

    IEnumerator GoToGameOverCoroutine()
    {
        // 3秒待ってからゲームオーバーシーンへ遷移
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("GameOverScene");
    }



    // ゴールポイントに入った時にEnemyを停止させ、画面をクリアSceneに遷移させる
    private void OnTriggerEnter(Collider other)
    {
        // Enemyを配列に格納
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        // 配列内のEnemyの追跡スクリプトを無効にする
        foreach(GameObject en in Enemies)
        {
            
        }


         
        
    }
    
    
    // 1秒待ってからクリア画面に遷移し、


}
