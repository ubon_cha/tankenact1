﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract　class EnemyMovePatrol : MonoBehaviour
{

    private NavMeshAgent _agent;
    private GameObject destination;                         // Agentの目的地に指定するゲームオブジェクトを格納
    public GameObject[] patrolPoints = new GameObject[4];   // Agentの目的地として登録するPatrolPointプレファブを指定
    public GameObject firstDestination;                     // Agentの最初の目的地となるゲームオブジェクトをInspectorビューで指定

    // Start is called before the first frame update
    void Start()
    {
        _agent = GetComponent<NavMeshAgent>();              // NavMeshAgentを保持しておく 
        destination = firstDestination;                     // destinationの初期化　最初の目的地となるゲームオブジェクトを代入する

    }

    // Update is called once per frame
    void Update()
    {
        _agent.destination = destination.transform.position;// agentの目的地にdestinationを代入する
    }

    // patrolPointsにたどり着いたらdestinationを切り替える
    /*private abstract void OnTriggerEnter()
    {
        OnTriggerEnterを抽象メソッドにしたい。
    }
    */
}
