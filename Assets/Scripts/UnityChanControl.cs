﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityChanControl : MonoBehaviour
{

    public float speed = 3f;
    float moveX = 0f;
    float moveZ = 0f;
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();


    }

    void Update()
    {

        moveX = Input.GetAxis("Vertical") * speed;
        moveZ = Input.GetAxis("Horizontal") * -speed;
        Vector3 direction = new Vector3(moveX, 0, moveZ);
    }

    void FixedUpdate()
    {
        rb.velocity = new Vector3(moveX, 0, moveZ);
    }
}

