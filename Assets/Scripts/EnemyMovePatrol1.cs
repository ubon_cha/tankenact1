﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(NavMeshAgent))]
public class EnemyMovePatrol1 : MonoBehaviour
{
    private NavMeshAgent _agent;
    private GameObject destination;  // Agentの目的地となるQuadオブジェクトを格納
    public GameObject[] patrolPoints = new GameObject[4];   // Agentの目的地として登録するPatrolPointプレファブ
      

    // Start is called before the first frame update
    void Start()
    {
        _agent = GetComponent<NavMeshAgent>();      // NavMeshAgentを保持しておく 
        destination = patrolPoints[0];      // destinationの初期化　最初にPatrolPoint1の座標を代入する
    }

    // patrolPointsにたどり着いたらdestinationを切り替える
    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "PatrolPoint1":
                destination = patrolPoints[1];  //destinationにPatrolPoint2を代入
                break;

            case "PatrolPoint2":
                destination = patrolPoints[2];  //destinationにPatrolPoint3を代入
                break;

            case "PatrolPoint3":
                destination = patrolPoints[3];  //destinationにPatrolPoint4を代入
                break;

            case "PatrolPoint4":
                destination = patrolPoints[0];  //destinationにPatrolPoint1を代入
                break;
        }
    }




    // Update is called once per frame
    void Update()
    {
        _agent.destination = destination.transform.position;
    }
}
